package uz.marokand.components

/**
 * @author Akbar Begimkulov
 * @since 2020, May 20
 */

class Circle(var radius: Double) : Shape {

    override fun getName(): String = "Circle"

    override fun getSurface(): Double = Math.PI * radius * radius
}