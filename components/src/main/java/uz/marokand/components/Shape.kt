package uz.marokand.components

/**
 * @author Akbar Begimkulov
 * @since 2020, May 20
 */

interface Shape {

    fun getName(): String

    fun getSurface(): Double
}