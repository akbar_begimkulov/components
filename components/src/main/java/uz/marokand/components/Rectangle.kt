package uz.marokand.components

/**
 * @author Akbar Begimkulov
 * @since 2020, May 20
 */

open class Rectangle(var a: Double, var b: Double) : Shape {

    override fun getName(): String = "Rectangle"

    override fun getSurface(): Double = a * b
}