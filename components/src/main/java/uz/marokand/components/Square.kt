package uz.marokand.components

/**
 * @author Akbar Begimkulov
 * @since 2020, May 20
 */
class Square(a: Double) : Rectangle(a, a) {

    override fun getName(): String = "Square"
}